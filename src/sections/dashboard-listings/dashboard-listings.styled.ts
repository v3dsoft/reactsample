import styled from 'styled-components';

import { DataCenterItem, SubsectionMenu } from 'controls';

export const ListingsSubsectionMenu = styled(SubsectionMenu)`
    margin: 0px -45px 30px -48px;
`;

export const ListingsBody = styled.div`
    display: flex;
    flex-wrap: wrap;
    display: relative;
`;

export const ListingsItem = styled(DataCenterItem)`
    margin-right: 21px;
    margin-bottom: 21px;
`;
export const DCRegistrationProgress = styled.div`
    width: 500px;
    height: 30px;
    background: #000;
`;

export const LoadingBody = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;

    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
`;
