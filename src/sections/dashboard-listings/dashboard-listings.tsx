import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { toast } from 'react-toastify';

import { LeftMenuIndices } from 'constants/menus';

import { IDataCenter, ILeaseConditionsItem } from 'api/api.types';

import {
    IDCListingItem,
    IDCListingCapacityItem,
    IDCListingLeaseConditionsItem,
} from 'store/store.types';

import { ICapacityItem } from 'api/api.types';

import {
    dcRegistrationStart,
    dcListingDeleteDC,
    dcListingUpdateDC,
    dcListingInit,
    dcListingUpdateStatus,
} from 'store/store.actions';
import {
    dcEditStart,
    dcEditGetDCFacility,
    dcEditUpdateDCLeaseConditions,
    dcSetRegistrationMode,
    dcEditUpdateDCcapacity,
} from 'store/dc/dc.actions';

import { RootStore } from 'store/store.reducer';

import { DashboardBase } from 'base-pages/dashboard-base';
import {
    ListingsBody,
    ListingsSubsectionMenu,
    LoadingBody,
} from './dashboard-listings.styled';
import { OverlayButton } from 'controls';

import { ReactComponent as WhitePlus } from 'images/WhitePlus.svg';

import { ResultsCards } from '../search-results/search-results.styled';
import { DCCard } from './dc-card';

import {
    DASHBOARD_LISTINGS_ROUTE,
    DASHBOARD_LISTINGS_PR_ROUTE,
} from 'constants/routes';
import { PulseLoader } from 'react-spinners';

const SUBSECTION_MENU_ITEMS = [
    {
        title: 'Listings',
        route: DASHBOARD_LISTINGS_ROUTE,
    },
    {
        title: 'Purchase Requests',
        route: DASHBOARD_LISTINGS_PR_ROUTE,
    },
];

export interface IDashboardListingsPageProps {
    token: string;
    loading: boolean;
    dcList: IDCListingItem[];
    capacityList: IDCListingCapacityItem[];
    leaseConditionsList: IDCListingLeaseConditionsItem[];
    dcRegistrationStart: () => void;
    dcSetRegistrationMode: () => void;
    dcEditStart: (dcId: number, mode: string) => void;
    dcEditGetDCFacility: (dcId: number) => void;
    dcEditUpdateDCcapacity: (dcCapacity: ICapacityItem) => void;
    dcEditUpdateDCLeaseConditions: (
        dcLeaseConditions: ILeaseConditionsItem
    ) => void;
    dcListingInit: (onError: (message: string) => void) => void;
    dcListingDeleteDC: (id: string, onError: (message: string) => void) => void;
    dcListingUpdateDC: (
        data: IDataCenter,
        onError: (message: string) => void
    ) => void;
    dcListingUpdateStatus: (
        id: number,
        status: string,
        onError: (message: string) => void
    ) => void;
}

export const DashboardListingsPageComponent = (
    props: IDashboardListingsPageProps
) => {
    const {
        dcListingInit,
        dcEditUpdateDCLeaseConditions,
        dcEditUpdateDCcapacity,
        dcEditStart,
        dcSetRegistrationMode,
        dcEditGetDCFacility,
    } = props;

    const handleError = (message: string) => {
        toast.error(message);
    };

    useEffect(() => {
        dcListingInit(handleError);
    }, [dcListingInit]);

    const handleAdd = () => {
        props.dcRegistrationStart();
        dcSetRegistrationMode();
    };

    const handleEditStart = async (id?: number) => {
        if (id) {
            const dcCapacity = props.capacityList.find(
                capacity => capacity.parentId === id
            );

            const dcLeaseConditions = props.leaseConditionsList.find(
                leaseCondition => leaseCondition.parentId === id
            );

            dcEditStart(id, 'edit');
            dcEditGetDCFacility(id);
            dcCapacity && dcEditUpdateDCcapacity(dcCapacity);
            dcLeaseConditions &&
                dcEditUpdateDCLeaseConditions(dcLeaseConditions);
        }
    };

    const handleDCActivate = (listingId: number) => {
        props.dcListingUpdateStatus(listingId, 'Active', handleError);
    };

    const handleDCInactivate = (listingId: number) => {
        props.dcListingUpdateStatus(listingId, 'Inactive', handleError);
    };

    const loggedIn = props.token !== '';

    const renderDCitem = () => {
        return props.dcList.map((dc, index) => {
            const dcLeaseConditions = props.leaseConditionsList.find(
                leaseCondition => leaseCondition.parentId === dc.id
            );

            const dcCapacity = props.capacityList.find(
                capacity => capacity.parentId === dc.id
            );

            const dcItem = {
                ...dc,
                capacity: dcCapacity,
                lease_condition: dcLeaseConditions,
            };
            return (
                <DCCard
                    key={`DCListItem_${index}`}
                    data={dcItem}
                    loggedIn={loggedIn}
                    onEdit={() => handleEditStart(dc.id)}
                    onActivate={handleDCActivate}
                    onInactivate={handleDCInactivate}
                />
            );
        });
    };

    return (
        <DashboardBase
            title="Data Center"
            currMenuIndex={LeftMenuIndices.Listings}
        >
            <ListingsSubsectionMenu
                items={SUBSECTION_MENU_ITEMS}
                currRoute={DASHBOARD_LISTINGS_ROUTE}
            />
            <ListingsBody>
                <ResultsCards>{renderDCitem()}</ResultsCards>
                {props.loading && (
                    <LoadingBody>
                        <PulseLoader color="#4A90E2" size={8} />
                    </LoadingBody>
                )}
            </ListingsBody>
            <OverlayButton onClick={handleAdd}>
                <WhitePlus />
            </OverlayButton>
        </DashboardBase>
    );
};

const mapStateToProps = (state: RootStore) => ({
    token: state.auth.token,
    loading: state.dcListing.loading,
    dcList: state.dcListing.dcList,
    capacityList: state.dcListing.capacityList,
    leaseConditionsList: state.dcListing.leaseConditionsList,
});

const dispatchProps = {
    dcRegistrationStart,
    dcEditStart,
    dcSetRegistrationMode,
    dcEditGetDCFacility,
    dcEditUpdateDCLeaseConditions,
    dcEditUpdateDCcapacity,
    dcListingInit,
    dcListingDeleteDC,
    dcListingUpdateDC,
    dcListingUpdateStatus,
};

export const DashboardListingsPage = connect(
    mapStateToProps,
    dispatchProps
)(DashboardListingsPageComponent);
