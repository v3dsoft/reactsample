import React, { useState } from 'react';
import {
    CardWrapper,
    DotsMenuWrapper,
    CardInfoWrapper,
    PowerPriceInfo,
    PowerInfo,
    AdditionalInfo,
    Info,
    MainInfo,
    SecondaryInfo,
    PowerPriceInfoWrapper,
    Image,
    InfoArrowWrapper,
    SimpleDropdownCard,
    StatusBarBox,
    StatusBarText,
    StatusBarPercentage,
    ProgressBarBody,
    ProgressBarBar,
} from './dc-card.styled';
import { useOnClickOutside } from 'base-pages/hooks/useOnClickOutside';

import { DotsMenu } from 'controls';
import Bitmap from 'images/Bitmap.png';
import BlueArrowRight from 'images/BlueArrowRight.svg';
import { DropdownMenu } from './dropdown-menu';

export interface IDCCardData {
    id?: number;
    name: string;
    lat?: number;
    lng?: number;
    address?: any;
    capacity?: any;
    lease_condition?: any;
    logo?: string;
    status?: string;
    listingId?: number;
    activated?: boolean;
    fieldNumCompleted?: number;
    fieldNumTotal?: number;
}

export interface IDCCardProps {
    data: IDCCardData;
    loggedIn?: boolean;
    options?: { value: string; label: string }[];
    onEdit?: () => void;
    onActivate?: (listingId: number) => void;
    onInactivate?: (listingId: number) => void;
}

export const DCCard = (props: IDCCardProps) => {
    const [openSort, setOpenSort] = useState(false);
    const node = React.useRef(null);

    const dotMenuItems = [
        { value: 'Statistics', label: 'Statistics' },
        { value: 'Modify', label: 'Modify' },
        props.data.activated
            ? { value: 'Inactivate', label: 'Inactivate' }
            : { value: 'Activate', label: 'Activate' },
        { value: 'Remove', label: 'Remove' },
        { value: 'Clone', label: 'Clone' },
    ];

    useOnClickOutside(node, () => {
        setOpenSort(false);
    });

    const handleMenuItemClick = (value: string) => {
        if (props.data.listingId) {
            if (value === 'Activate' && props.onActivate) {
                props.onActivate(props.data.listingId);
            } else if (value === 'Inactivate' && props.onInactivate) {
                props.onInactivate(props.data.listingId);
            }
        }
    };

    const renderDCLocation = () => {
        const isUSA =
            props.data.address &&
            props.data.address.country === 'United States';

        if (isUSA) {
            return `${props.data.address.city}, ${props.data.address.province}`;
        }

        return props.data.address.country && props.data.address.city
            ? `${props.data.address.country}, ${props.data.address.city}`
            : props.data.address.country;
    };

    const pricePerKWT =
        props.data.lease_condition?.pricing?.price_per_kwt || 345;

    let finishedPercentage = 0;
    if (props.data.fieldNumCompleted && props.data.fieldNumTotal) {
        finishedPercentage = Math.ceil(
            (props.data.fieldNumCompleted / props.data.fieldNumTotal) * 100
        );
    }

    return (
        <CardWrapper openSort={openSort}>
            <DotsMenuWrapper onClick={() => setOpenSort(!openSort)}>
                <DotsMenu />
            </DotsMenuWrapper>
            <CardInfoWrapper>
                <PowerPriceInfoWrapper>
                    {props.data.logo ? (
                        <Image src={props.data.logo} />
                    ) : (
                        <Image src={Bitmap} />
                    )}
                    <PowerPriceInfo>
                        <PowerInfo>{`$${parseFloat(
                            pricePerKWT
                        )}/kW`}</PowerInfo>
                        <AdditionalInfo>annual</AdditionalInfo>
                    </PowerPriceInfo>
                </PowerPriceInfoWrapper>

                <InfoArrowWrapper>
                    <Info>
                        <MainInfo>{renderDCLocation()}</MainInfo>

                        {props.data.capacity &&
                        props.data.capacity.available_power_capacity ? (
                            <SecondaryInfo>
                                Avail. Capacity:{' '}
                                {props.data.capacity.available_power_capacity}{' '}
                                kW <br /> Availbility: Immediate <br />{' '}
                            </SecondaryInfo>
                        ) : (
                            <SecondaryInfo>
                                Avail. Capacity: 1.2 kW <br /> Availbility:
                                Immediate <br />{' '}
                            </SecondaryInfo>
                        )}
                    </Info>
                    <Image src={BlueArrowRight} onClick={props.onEdit} />
                </InfoArrowWrapper>
                <StatusBarBox>
                    <StatusBarText>
                        Your DC profile is completed on{' '}
                        <StatusBarPercentage>
                            {finishedPercentage}%
                        </StatusBarPercentage>
                    </StatusBarText>
                    <ProgressBarBody>
                        <ProgressBarBar percentage={finishedPercentage} />
                    </ProgressBarBody>
                </StatusBarBox>
            </CardInfoWrapper>
            <SimpleDropdownCard openSort={openSort} ref={node}>
                <DropdownMenu
                    options={dotMenuItems}
                    onItemClick={handleMenuItemClick}
                    closeModal={() => setOpenSort(false)}
                />
            </SimpleDropdownCard>
        </CardWrapper>
    );
};
