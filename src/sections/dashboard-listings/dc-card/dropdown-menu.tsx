import React from 'react';
import { Item } from './dropdown-menu.styled';

export interface IDropdownValue {
    value: string;
    label: string;
}

export interface ISimpleDropdownProps {
    options?: IDropdownValue[];
    value?: string;
    subOptionsDistance?: IDropdownValue[] | undefined;
    subOptionsPrice?: IDropdownValue[];
    onItemClick?: (value: string) => void;
    closeModal?: () => void;
}

export const DropdownMenu = (props: ISimpleDropdownProps) => {
    const handleDropdownItemClick = (value: string) => {
        props.onItemClick!(value);
        props.closeModal!();
    };

    const dropDownOptions = props.options ? props.options : [];

    return (
        <>
            {dropDownOptions.map(item => {
                return (
                    <Item
                        key={item.label}
                        onClick={() => handleDropdownItemClick(item.value)}
                    >
                        {item.label}
                    </Item>
                );
            })}
        </>
    );
};
