import styled from 'styled-components';

export const Item = styled.div`
    border-bottom: 1px solid rgba(123, 123, 123, 0.42);
    width: 100%;
    height: 53px;
    padding: 15px 0;
    text-align: center;
    cursor: pointer;
    &:last-child {
        border-bottom: none;
    }
    &:hover {
        background: #498FE1;
        color: #ffffff;
        border-bottom: none;
        
    }
    &:last-child:hover {
        border-radius: 0 0 8px 8px;
    }
    &:first-child:hover {
        border-radius: 8px 8px 0 0;
    }
`;

export const SubMenuWrapper = styled.ul`
    display: none;

`

export const SubItem = styled.li`

`
