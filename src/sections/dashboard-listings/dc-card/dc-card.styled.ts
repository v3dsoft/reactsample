import styled from 'styled-components';

export const DropdownBase = styled.div<{ openSort?: boolean }>`
    visibility: ${props => (props.openSort ? 'visible' : 'hidden')};
    border: ${props => (props.openSort ? 'red' : 'black')};
    position: absolute;
    background: #ffffff;
    box-shadow: 0px 8px 40px rgba(0, 0, 0, 0.12);
    border-radius: 8px;
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 190px;
    right: -2px;
    top: 25px;
    cursor: pointer;
    justify-content: space-between;
    &::before {
        content: '';
        position: absolute;
        right: 14px;
        top: -7px;
        width: 0;
        height: 0;
        border-style: solid;
        border-width: 0 7px 7px 7px;
        border-color: transparent transparent #ffffff transparent;
        z-index: 9998;
    }
`;

export const SimpleDropdownCard = styled(DropdownBase)`
    top: 30px;
    &::before {
        right: 20px;
    }
`;

export const CardWrapper = styled.div<{
    openSort?: boolean;
    selected?: boolean;
}>`
    background: ${props =>
        props.openSort ? 'rgba(74, 144, 226, 0.4)' : '#ffffff'};
    font-family: 'AvenirNext', sans-serif;
    border-radius: 16px;
    padding: 50px 15.77px 0px 20px;
    position: relative;
    box-sizing: border-box;
    overflow: hidden;
    @media (max-width: 768px) {
        padding: 17px 13.5px;
    }
`;

export const DotsMenuWrapper = styled.div`
    height: fit-content;
    justify-self: flex-end;
    position: absolute;
    top: 1px;
    right: 12px;
    @media (max-width: 768px) {
        display: none;
    }
`;

export const Image = styled.img`
    max-width: 90px;
    /* max-height: 90px; */
    border-radius: 10px;
`;
export const PowerPriceInfoWrapper = styled.div`
    display: flex;
    justify-content: space-between;
    @media (max-width: 768px) {
        align-items: center;
    }
`;
export const CardInfoWrapper = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    height: 100%;
`;

export const PowerPriceInfo = styled.div`
    display: flex;
    flex-direction: column;
`;

export const PowerInfo = styled.span`
    font-size: 28px;
    line-height: 33px;
    text-align: right;

    color: #0fb269;
`;
export const AdditionalInfo = styled(PowerInfo)`
    font-size: 17px;
    line-height: 20px;
`;
export const Info = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    margin-top: 12px;
`;
export const MainInfo = styled.div`
    font-size: 16px;
    line-height: 16px;

    color: #212145;
    
    margin-bottom: 5px;
`;
export const SecondaryInfo = styled.div`
    font-size: 15px;
    line-height: 18px;

    color: #9191af;
`;

export const InfoArrowWrapper = styled(PowerPriceInfoWrapper)`
    display: flex;
    justify-content: space-between;
    img {
        /* position: absolute;
    right: 15.96px;
    bottom: 31.86px; */
        align-self: flex-end;
        cursor: pointer;
    }
`;

export const StatusBarBox = styled.div`
    background: #596fa7;
    height: 72px;
    margin-left: -20px;
    margin-right: -16px;
    margin-top: 5px;
    padding: 10px 20px;
`;

export const StatusBarText = styled.div`
    font-weight: 500;
    font-size: 12px;
    line-height: 19px;
    color: white;
    margin-bottom: 7px;
`;

export const StatusBarPercentage = styled.span`
    font-weight: bold;
`;

export const ProgressBarBody = styled.div`
    width: 100%;
    height: 6px;
    border-radius: 3px;
    background-color: #e4f1ff;
`;

export const ProgressBarBar = styled.div<{
    percentage: number;
}>`
    width: ${props => props.percentage}%;
    height: 6px;
    border-radius: 3px;
    background-color: ${props =>
        props.percentage < 100 ? '#4A90E2' : '#7ED321'};
`;
