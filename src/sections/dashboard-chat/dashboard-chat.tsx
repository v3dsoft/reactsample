import React, { useState, useEffect, useRef } from 'react';
import { toast } from 'react-toastify';
import { connect } from 'react-redux';
import { RootStore } from 'store/store.reducer';
import { useParams } from 'react-router-dom';

import {
    ChatBody,
    ChatHead,
    ChatMain,
    InputField,
    HeadName,
    Star,
    Message,
    MessageText,
    MessagesWrapper,
    ChatInput,
    SendButton,
    ChatInputWrapper,
    SendButtonMobile,
    NameAndStatusWrap,
    Status,
    Name,
    ScrollMarker,
} from './dashboard-chat.styled';
import { ReactComponent as Smile } from 'images/Smile.svg';
import { ReactComponent as SendImg } from 'images/Send.svg';

import { DashboardBase } from 'base-pages/dashboard-base';
import { LeftMenuIndices } from 'constants/menus';
import { ReactComponent as StarImg } from 'images/Star.svg';

import { IMessageListItem } from 'store/store.types';
import {
    messagesInitMessageList,
    messagesSetMessageList,
    messagesSendMessage,
    MessageActionType,
} from 'store/store.actions';

export interface IChatRoom {
    currChatCompanion: string;
    messageList: IMessageListItem[];
    myEmail: string;

    messagesSetMessageList: (
        messageList: IMessageListItem[]
    ) => MessageActionType;
    messagesInitMessageList: (
        chatId: string,
        myEmail: string,
        onOk?: () => void,
        onError?: (message: string) => void
    ) => void;
    messagesSendMessage: (
        chatId: string,
        message: string,
        onOk?: () => void,
        onError?: (message: string) => void
    ) => void;
}

const renderMessageGroup = (messageList: IMessageListItem[], my: boolean) => {
    if (messageList.length > 0) {
        return (
            <Message key={`msgGr_${messageList[0].id}`} msgFrom={!my}>
                <Name>{my ? 'Me:' : messageList[0].sender_name}</Name>
                {messageList.map(message => (
                    <MessagesWrapper msgFrom={!my}>
                        <MessageText msgFrom={!my}>
                            {message.message}
                        </MessageText>
                    </MessagesWrapper>
                ))}
            </Message>
        );
    } else {
        return null;
    }
};

const renderMessages = (messageList: IMessageListItem[], myEmail: string) => {
    console.log(`myEmail: ${myEmail}`);

    if (messageList.length > 0) {
        const messages: React.ReactNode[] = [];
        let messageGroup: IMessageListItem[] = [];
        let groupSender = messageList[0].sender_mail;

        messageList.forEach(message => {
            if (message.sender_mail !== groupSender) {
                if (messageGroup.length > 0) {
                    messages.push(
                        renderMessageGroup(
                            messageGroup,
                            messageGroup[0].sender_mail === myEmail
                        )
                    );
                }

                messageGroup = [];
                groupSender = message.sender_mail;
            }

            messageGroup.push(message);
        });

        if (messageGroup.length > 0) {
            messages.push(
                renderMessageGroup(
                    messageGroup,
                    messageGroup[0].sender_mail === myEmail
                )
            );
        }

        return messages;
    } else {
        return [];
    }
};

export const ChatRoom = (props: IChatRoom) => {
    const [message, updateMessage] = useState('');

    const scrollMarker = useRef<HTMLDivElement>(null);

    const { id } = useParams();
    const { myEmail, messagesSetMessageList, messagesInitMessageList } = props;
    useEffect(() => {
        if (id) {
            messagesInitMessageList(
                id,
                myEmail,

                () => {
                    if (scrollMarker && scrollMarker.current) {
                        scrollMarker.current.scrollIntoView({
                            behavior: 'smooth',
                        });
                    }
                },

                message => {
                    toast.error(message);
                }
            );
        } else {
            messagesSetMessageList([]);
        }
    }, [id, myEmail, messagesSetMessageList, messagesInitMessageList]);

    const handleError = (message: string) => {
        toast.error(message);
    };

    const submitMessage = () => {
        if (id && message) {
            props.messagesSendMessage(
                id,
                message,

                () => {
                    updateMessage('');
                    messagesInitMessageList(
                        id,
                        myEmail,

                        () => {
                            if (scrollMarker && scrollMarker.current) {
                                scrollMarker.current.scrollIntoView({
                                    behavior: 'smooth',
                                });
                            }
                        },

                        errMsg => {
                            toast.error(errMsg);
                        }
                    );

                    if (scrollMarker && scrollMarker.current) {
                        scrollMarker.current.scrollIntoView({
                            behavior: 'smooth',
                        });
                    }
                },

                handleError
            );
        }
    };

    const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        updateMessage(e.target.value);
    };

    return (
        <DashboardBase
            title="Messages"
            currMenuIndex={LeftMenuIndices.Dashboard}
        >
            <ChatBody>
                <ChatHead>
                    <NameAndStatusWrap>
                        <Status />
                        <HeadName>{props.currChatCompanion}</HeadName>
                    </NameAndStatusWrap>
                    <Star>
                        <StarImg />
                    </Star>
                </ChatHead>
                <ChatMain>
                    {renderMessages(props.messageList, props.myEmail)}
                    <ScrollMarker ref={scrollMarker} />
                </ChatMain>
                <InputField>
                    <ChatInputWrapper>
                        <ChatInput
                            type="text"
                            placeholder="Type a message here…"
                            value={message}
                            onChange={handleInputChange}
                        />
                        <Smile className="smile" />
                    </ChatInputWrapper>
                    <SendButton disabled={!message} onClick={submitMessage}>
                        Send
                    </SendButton>
                    <SendButtonMobile
                        disabled={!message}
                        onClick={submitMessage}
                    >
                        <SendImg className="send" />
                    </SendButtonMobile>
                </InputField>
            </ChatBody>
        </DashboardBase>
    );
};

const mapStateToProps = (state: RootStore) => ({
    currChatCompanion: state.messages.currChatCompanion,
    messageList: state.messages.messageList,
    myEmail: state.auth.mail,
});

const dispatchProps = {
    messagesSetMessageList,
    messagesInitMessageList,
    messagesSendMessage,
};

export const Chat = connect(mapStateToProps, dispatchProps)(ChatRoom);
