import styled from 'styled-components';

export const ChatBody = styled.div`
    margin: 0 -45px 0 -48px;
    height: calc(100vh - 92px);
    @media (max-width: 850px) {
        height: calc(100vh - 169px);
    }
`;
export const ChatHead = styled.div`
    height: 66px;
    background: #f9f9f9;
    padding: 12px 24px 12px 26px;
    display: flex;
    align-items: center;
    justify-content: space-between;
`;
export const AvatarWrapper = styled.div`
    position: relative;
    &:after {
        content: '';
        position: absolute;
        width: 12px;
        height: 12px;
        min-width: 12px;
        background: #2dcd7a;
        right: -2px;
        bottom: 2px;
        border-radius: 50%;
        border: 1.5px solid #fff;
    }
`;
export const Avatar = styled.img`
    width: 44px;
    height: 44px;
    min-width: 44px;
    border-radius: 14px;
    position: relative;
`;
export const HeadName = styled.div`
    font-weight: 600;
    font-size: 16px;
    color: #2c2c2c;
    display: flex;
    margin-left: 10px;
    width: 100%;
`;
export const Status = styled.div`
    background: #2dcd7a;
    width: 9px;
    height: 9px;
    border-radius: 50%;
`;
export const NameAndStatusWrap = styled.div`
    display: flex;
    align-items: center;
`;
export const Star = styled.div`
    width: 19px;
    height: 19px;
`;
export const ChatMain = styled.div`
    height: calc(100% - 133px);
    width: 100%;
    padding: 1px 45px 0 38px;
    overflow-y: auto;
    @media (min-width: 768px) {
        scrollbar-color: #4a90e2 #d2d2d2;
        scrollbar-width: thin;
        &::-webkit-scrollbar {
            width: 10px;
        }
        &::-webkit-scrollbar-track {
            background: #d2d2d2;
            /* border-radius: 10px; */
        }
        &::-webkit-scrollbar-thumb {
            background: #4a90e2;
            /* border-radius: 10px; */
        }
    }
    @media (max-width: 500px) {
        padding: 1px 20px 0;
    }
`;
export const Message = styled.div<{ msgFrom?: boolean }>`
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: ${props => (props.msgFrom ? 'flex-start' : 'flex-end')};
    > h3 {
        color: ${props => (props.msgFrom ? '#2C2C2C' : '#4A90E2')};
    }
    &:last-child {
        margin-bottom: 30px;
    }
`;
export const Name = styled.h3`
    font-style: normal;
    font-weight: 500;
    font-size: 16px;
    line-height: 19px;
    color: #2c2c2c;
`;
export const MessagesWrapper = styled.div<{ msgFrom?: boolean }>`
    display: flex;
    flex-direction: column;
    margin-bottom: 12px;
    &:last-child {
        margin-bottom: 0;
    }
`;
export const MessageText = styled.div<{ msgFrom?: boolean }>`
    font-size: 16px;
    line-height: 24px;
    color: ${props => (props.msgFrom ? '#2C2C2C' : '#FFFFFF')};
    ${props =>
        props.msgFrom
            ? 'padding: 9px 17px 9px 27px'
            : 'padding: 9px 14px 9px 16px'};
    border-radius: 8px;
    width: fit-content;
    background: ${props =>
        props.msgFrom
            ? '#fff'
            : 'linear-gradient(90deg, #7CAFEB 5.14%, #4A90E2 100%);'};
    margin-top: 12px;
    &:first-child {
        margin-top: 0;
    }
    ${props =>
        props.msgFrom
            ? 'background: #fff'
            : 'background: linear-gradient(90deg, #7CAFEB 5.14%, #4A90E2 100%)'};
    box-shadow: 0px 3px 6px rgba(0, 0, 0, 0.227437);
    height: fit-content;
    max-width: 634px;
`;
export const InputField = styled.div`
    background: #fff;
    height: 67px;
    padding: 8px 10px 8px;
    display: flex;
    justify-content: space-between;
`;
export const ChatInputWrapper = styled.div`
    height: 100%;
    width: 100%;
    margin-right: 10px;
    position: relative;
    .smile {
        position: absolute;
        right: 28px;
        top: 50%;
        transform: translateY(-50%);
        cursor: pointer;
    }
    @media (max-width: 768px) {
        width: 100%;
    }
`;
export const ChatInput = styled.input`
    height: 100%;
    width: 100%;
    background: #f9f9f9;
    border: 1px solid #dfe4ea;
    border-radius: 25px;
    font-size: 21px;
    line-height: 22px;
    padding: 0 20px;
    &:focus {
        outline: none;
    }
    @media (max-width: 400px) {
        font-size: 15px;
    }
`;
export const SendButton = styled.button`
    width: 164px;
    min-width: 164px;
    height: 100%;
    background: #4a90e2;
    border-radius: 25px;
    border: none;
    color: #fff;
    font-size: 18px;
    line-height: 22px;
    font-weight: normal;
    cursor: pointer;
    @media (max-width: 768px) {
        display: none;
    }
`;
export const SendButtonMobile = styled(SendButton)`
    width: 51px;
    min-width: 51px;
    display: flex;
    justify-content: center;
    align-items: center;
    .send {
        height: 23px;
        margin: 4px 4px 0 0;
    }
    @media (max-width: 768px) {
        display: block;
    }
    @media (min-width: 769px) {
        display: none;
    }
`;

export const ScrollMarker = styled.div`
    margin-top: 10px;
`;
