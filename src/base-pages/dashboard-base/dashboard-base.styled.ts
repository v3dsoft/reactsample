import styled from 'styled-components';
import { Link } from 'react-router-dom';

import { EventsIcon } from 'controls';
import { ReactComponent as MenuIcon } from '../../images/MenuIconMobile.svg';
import { ReactComponent as MenuRightDropDown } from '../../images/MenuRightDropDown.svg';

export const DashboardBaseBody = styled.div`
    display: flex;
    align-items: stretch;
    min-height: 100vh;
    position: relative;
`;

export const LeftMenuBody = styled.div<{ open?: boolean }>`
    width: 240px;
    background: #dce1ee;
    box-shadow: inset -10px 0px 30px rgba(85, 99, 159, 0.02);
    display: flex;
    flex-direction: column;
    align-items: stretch;
    @media (max-width: 850px) {
        transform: ${({ open }) =>
            open ? 'translateX(0)' : 'translateX(-100%)'};
        transition: transform 0.3s ease-in-out;
        height: 100vh;
        z-index: 100;
        position: absolute;
    }
`;

export const TitleIconBox = styled.div`
    padding: 27px 35px 42px 44px;
    cursor: pointer;
`;

export const LeftMenuLink = styled(Link)`
    text-decoration: none;
`;

export const LeftMenuItemBody = styled.div`
    padding-left: 44px;
    margin-right: 20px;
    margin-bottom: 4px;
    height: 38px;
    border-radius: 0 19px 19px 0;
    display: flex;
    align-items: center;
    cursor: pointer;
    &:hover {
        background: rgba(238, 242, 246, 0.5);
        mix-blend-mode: normal;
    }
    &:hover path {
        fill: #4a90e2;
    }
    &:hover > div {
        font-weight: 500;
        color: #4a90e2;
    }
`;

export const LeftMenuItemBodyActive = styled(LeftMenuItemBody)`
    background: rgba(238, 242, 246, 0.5);
    mix-blend-mode: normal;
    & path {
        fill: #4a90e2;
    }
`;

export const LeftMenuItemText = styled.div`
    margin-left: 20px;
    font-style: normal;
    font-weight: normal;
    font-size: 15px;
    line-height: 18px;
    color: #565682;
    text-decoration: none !important;
`;

export const LeftMenuItemTextActive = styled(LeftMenuItemText)`
    font-weight: 500;
    color: #4a90e2;
`;

export const ContentBody = styled.div`
    flex: 1;
    display: flex;
    flex-direction: column;
    align-items: stretch;
    padding: 0 45px 0 48px;
    position: relative;
`;

export const HeaderBody = styled.div`
    height: 92px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    width: calc(100% + 92px);
    margin-left: -48px;
    padding: 0 23px;
`;

export const BackButton = styled.button`
    margin-right: 32px;
    background: #4a90e2;
    width: 19px;
    height: 19px;
    border: none;
    border-radius: 50%;
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;
`;

export const BackIconBox = styled.div`
    position: relative;
    left: -1px;
`;

export const Title = styled.div`
    flex: 1;
    font-size: 24px;
    line-height: 28px;
    color: #4a90e2;
`;

export const TopSearchLink = styled(Link)`
    cursor: pointer;
    @media (max-width: 850px) {
        display: none;
    }
`;

export const UserEventsIcon = styled(EventsIcon)`
    margin: 0 32px;
    @media (max-width: 850px) {
        display: none;
    }
`;

export const MobileHeader = styled.div`
    display: none;
    @media (max-width: 850px) {
        width: 100%;
        display: flex;
        justify-content: space-between;
        margin-top: 25px;
        align-items: center;
    }
`;

export const MenuRightDropDownMobile = styled(MenuRightDropDown)`
    display: block;
`;
export const MenuIconMobile = styled(MenuIcon)<{ open?: boolean }>`
    display: none;
    cursor: pointer;
    margin-right: 24px;
    @media (max-width: 850px) {
        display: block;
    }
`;
