import React, { useState, useRef, FunctionComponent } from 'react';
import { Link } from 'react-router-dom';

import { ROOT_ROUTE } from 'constants/routes';
import { LEFT_MENU_ITEMS } from 'constants/menus';
import { UserMenu, SimplePopup, PopupPasswordChange } from 'controls';
import {
    DashboardBaseBody,
    LeftMenuBody,
    ContentBody,
    HeaderBody,
    Title,
    UserEventsIcon,
    TopSearchLink,
    TitleIconBox,
    LeftMenuLink,
    BackButton,
    BackIconBox,
    MenuIconMobile,
} from './dashboard-base.styled';
import { LeftMenuItem } from './dashboard-base.left-menu-item';

import { ReactComponent as EdgevanaLogo } from 'images/EdgevanaLogo.svg';
import { ReactComponent as BackIcon } from 'images/BackIcon.svg';
import { ReactComponent as TopSearchIcon } from 'images/TopSearchIcon.svg';
import { useOnClickOutside } from '../hooks/useOnClickOutside';

export interface IDashboardBaseProps {
    title: string;
    currMenuIndex: number;
    onBack?: () => void;
    open?: true;
}

export const DashboardBase: FunctionComponent<IDashboardBaseProps> = props => {
    const [open, setOpen] = useState(false);
    const [isPasswordPopupOpen, showPasswordPopup] = useState(false);
    const node = useRef(null);

    useOnClickOutside(node, () => {
        setOpen(false);
    });

    const closePasswordPopup = () => showPasswordPopup(false);
    const openPasswordPopup = () => showPasswordPopup(true);

    return (
        <DashboardBaseBody>
            <LeftMenuBody open={open} ref={node}>
                <Link to={ROOT_ROUTE}>
                    <TitleIconBox>
                        <EdgevanaLogo />
                    </TitleIconBox>
                </Link>
                {LEFT_MENU_ITEMS.map((item, index) => {
                    if (index !== props.currMenuIndex && !item.isWithPopup) {
                        return (
                            <LeftMenuLink
                                key={`leftMnu_${index}`}
                                to={item.route}
                            >
                                <LeftMenuItem
                                    icon={item.icon}
                                    name={item.name}
                                />
                            </LeftMenuLink>
                        );
                    } else if (item.isWithPopup) {
                        return (
                            <LeftMenuItem
                                key={`leftMnu_${index}`}
                                icon={item.icon}
                                name={item.name}
                                onClick={() => openPasswordPopup()}
                            />
                        );
                    } else {
                        return (
                            <LeftMenuItem
                                key={`leftMnu_${index}`}
                                icon={item.icon}
                                name={item.name}
                                active
                            />
                        );
                    }
                })}
            </LeftMenuBody>
            <ContentBody>
                <HeaderBody>
                    <MenuIconMobile
                        open={open}
                        onClick={() => {
                            setOpen(!open);
                        }}
                    />
                    {props.onBack && (
                        <BackButton onClick={props.onBack}>
                            <BackIconBox>
                                <BackIcon />
                            </BackIconBox>
                        </BackButton>
                    )}
                    <Title>{props.title}</Title>
                    <TopSearchLink to={ROOT_ROUTE}>
                        <TopSearchIcon />
                    </TopSearchLink>
                    <UserEventsIcon />
                    <UserMenu />
                </HeaderBody>
                {props.children}
            </ContentBody>
            <SimplePopup
                open={isPasswordPopupOpen}
                onClose={closePasswordPopup}
                title="Password Change"
            >
                <PopupPasswordChange />
            </SimplePopup>
        </DashboardBaseBody>
    );
};
