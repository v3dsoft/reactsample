import React from 'react';
import {
    LeftMenuItemBody,
    LeftMenuItemText,
    LeftMenuItemBodyActive,
    LeftMenuItemTextActive,
} from './dashboard-base.styled';

export interface ILeftMenuItemProps {
    icon: React.ReactNode;
    name: string;
    active?: boolean;
    onClick?: () => void;
}

export const LeftMenuItem = (props: ILeftMenuItemProps) => {
    if (props.active) {
        return (
            <LeftMenuItemBodyActive>
                {props.icon}
                <LeftMenuItemTextActive>{props.name}</LeftMenuItemTextActive>
            </LeftMenuItemBodyActive>
        );
    }

    return (
        <LeftMenuItemBody onClick={props.onClick}>
            {props.icon}
            <LeftMenuItemText>{props.name}</LeftMenuItemText>
        </LeftMenuItemBody>
    );
};
